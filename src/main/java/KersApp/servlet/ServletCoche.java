package KersApp.servlet;

import KersApp.modelo.Circuito;
import KersApp.modelo.Coche;
import KersApp.modelo.dao.CocheDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Javier González de Lope
 */
public class ServletCoche extends HttpServlet {

    private CocheDAO cocheDAO;

    @Override
    public void init() {
        String urlDB = getServletContext().getInitParameter("urlDB");
        String usuarioDB = getServletContext().getInitParameter("usuarioDB");
        String claveDB = getServletContext().getInitParameter("claveDB");

        cocheDAO = new CocheDAO(urlDB, usuarioDB, claveDB);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getRequestURI();

        try {
            switch (action) {
                case "/KersApp/coches/nuevo":
                    mostrarFormularioCreacion(request, response);
                    break;
                case "/KersApp/coches/editar":
                    mostrarFormularioEdicion(request, response);
                    break;
                case "/KersApp/coches/eliminar":
                    eliminarCoche(request, response);
                    break;
                default:
                    listarCoches(request, response);
            }
        } catch (SQLException ex) {
            System.out.println("Error en Servlet Coche (GET):" + ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getRequestURI();

        try {
            switch (action) {
                case "/KersApp/coches/insertar":
                    insertarCoche(request, response);
                    break;
                case "/KersApp/coches/actualizar":
                    actualizarCoche(request, response);
                    break;
                default:
                    listarCoches(request, response);
            }
        } catch (SQLException ex) {
            System.out.println("Error en Servlet Coche (POST):" + ex);
        }
    }

    private void insertarCoche(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {

        HttpSession session = request.getSession();

        String nombre = request.getParameter("nombre");
        int gananciaPorCurva = Integer.parseInt(request.getParameter("gananciaPorCurva"));

        Coche coche = new Coche(nombre, gananciaPorCurva);
        if (cocheDAO.insertar(coche)) {
            session.setAttribute("alerta", "El coche se ha creado con éxito.");
            session.setAttribute("tipoAlerta", "success");
        } else {
            session.setAttribute("alerta", "Ha ocurrido un error al intentar crear el coche.");
            session.setAttribute("tipoAlerta", "error");
        }

        String location = request.getContextPath() + "/coches";
        response.sendRedirect(location);
    }

    private void actualizarCoche(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {

        HttpSession session = request.getSession();

        int id = Integer.parseInt(request.getParameter("id"));
        String nombre = request.getParameter("nombre");
        int gananciaPorCurva = Integer.parseInt(request.getParameter("gananciaPorCurva"));

        Coche coche = new Coche(id, nombre, gananciaPorCurva);
        if (cocheDAO.modificar(coche)) {
            session.setAttribute("alerta", "El coche se ha modificado con éxito.");
            session.setAttribute("tipoAlerta", "success");
        } else {
            session.setAttribute("alerta", "Ha ocurrido un error al intentar modificar el coche.");
            session.setAttribute("tipoAlerta", "error");
        }

        String location = request.getContextPath() + "/coches";
        response.sendRedirect(location);
    }

    private void eliminarCoche(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        HttpSession session = request.getSession();

        int id = Integer.parseInt(request.getParameter("id"));
        Coche circuito = new Coche(id);

        if (cocheDAO.eliminar(circuito)) {
            session.setAttribute("alerta", "El coche se ha eliminado con éxito.");
            session.setAttribute("tipoAlerta", "success");
        } else {
            session.setAttribute("alerta", "Ha ocurrido un error al intentar eliminar el coche.");
            session.setAttribute("tipoAlerta", "error");
        }

        String location = request.getContextPath() + "/coches";
        response.sendRedirect(location);
    }

    private void listarCoches(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        List<Coche> coches = cocheDAO.obtenerTodos();
        request.setAttribute("coches", coches);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/VistaCoche.jsp");
        rd.forward(request, response);
    }

    private void mostrarFormularioCreacion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/VistaCocheForm.jsp");
        request.setAttribute("action", "insertar");
        dispatcher.forward(request, response);
    }

    private void mostrarFormularioEdicion(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        String paramValue = request.getParameter("id");
        Integer id = Integer.valueOf(paramValue);
        Coche coche = cocheDAO.obtener(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/VistaCocheForm.jsp");
        request.setAttribute("coche", coche);
        request.setAttribute("action", "actualizar");
        dispatcher.forward(request, response);
    }

}
