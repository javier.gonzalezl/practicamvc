package KersApp.servlet;

import KersApp.modelo.Circuito;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import KersApp.modelo.dao.CircuitoDAO;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Javier González de Lope
 */
public class ServletCircuito extends HttpServlet {

    private CircuitoDAO circuitoDAO;

    @Override
    public void init() {
        String urlDB = getServletContext().getInitParameter("urlDB");
        String usuarioDB = getServletContext().getInitParameter("usuarioDB");
        String claveDB = getServletContext().getInitParameter("claveDB");

        circuitoDAO = new CircuitoDAO(urlDB, usuarioDB, claveDB);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getRequestURI();

        try {
            switch (action) {
                case "/KersApp/circuitos/nuevo":
                    mostrarFormularioCreacion(request, response);
                    break;
                case "/KersApp/circuitos/editar":
                    mostrarFormularioEdicion(request, response);
                    break;
                case "/KersApp/circuitos/eliminar":
                    eliminarCircuito(request, response);
                    break;
                default:
                    listarCircuitos(request, response);
            }
        } catch (SQLException ex) {
            System.out.println("Error en Servlet Circuito (GET):" + ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getRequestURI();

        try {
            switch (action) {
                case "/KersApp/circuitos/insertar":
                    insertarCircuito(request, response);
                    break;
                case "/KersApp/circuitos/actualizar":
                    actualizarCircuito(request, response);
                    break;
                default:
                    listarCircuitos(request, response);
            }
        } catch (SQLException ex) {
            System.out.println("Error en Servlet Circuito (POST):" + ex);
        }
    }

    private void insertarCircuito(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {

        HttpSession session = request.getSession();

        String nombre = request.getParameter("nombre");
        String ciudad = request.getParameter("ciudad");
        String pais = request.getParameter("pais");
        int numVueltas = Integer.parseInt(request.getParameter("numVueltas"));
        int longVuelta = Integer.parseInt(request.getParameter("longVuelta"));
        int curvasVuelta = Integer.parseInt(request.getParameter("curvasVuelta"));

        Circuito circuito = new Circuito(nombre, ciudad, pais, numVueltas, longVuelta, curvasVuelta);
        if (circuitoDAO.insertar(circuito)) {
            session.setAttribute("alerta", "El circuito se ha creado con éxito.");
            session.setAttribute("tipoAlerta", "success");
        } else {
            session.setAttribute("alerta", "Ha ocurrido un error al intentar crear el circuito.");
            session.setAttribute("tipoAlerta", "error");
        };

        String location = request.getContextPath() + "/circuitos";
        response.sendRedirect(location);
    }

    private void actualizarCircuito(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {

        HttpSession session = request.getSession();

        int id = Integer.parseInt(request.getParameter("id"));
        String nombre = request.getParameter("nombre");
        String ciudad = request.getParameter("ciudad");
        String pais = request.getParameter("pais");
        int numVueltas = Integer.parseInt(request.getParameter("numVueltas"));
        int longVuelta = Integer.parseInt(request.getParameter("longVuelta"));
        int curvasVuelta = Integer.parseInt(request.getParameter("curvasVuelta"));

        Circuito circuito = new Circuito(id, nombre, ciudad, pais, numVueltas, longVuelta, curvasVuelta);
        if (circuitoDAO.modificar(circuito)) {
            session.setAttribute("alerta", "El circuito se ha modificado con éxito.");
            session.setAttribute("tipoAlerta", "success");
        } else {
            session.setAttribute("alerta", "Ha ocurrido un error al intentar modificar el circuito.");
            session.setAttribute("tipoAlerta", "error");
        };

        String location = request.getContextPath() + "/circuitos";
        response.sendRedirect(location);
    }

    private void eliminarCircuito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        HttpSession session = request.getSession();

        int id = Integer.parseInt(request.getParameter("id"));
        Circuito circuito = new Circuito(id);

        if (circuitoDAO.eliminar(circuito)) {
            session.setAttribute("alerta", "El circuito se ha eliminado con éxito.");
            session.setAttribute("tipoAlerta", "success");
        } else {
            session.setAttribute("alerta", "Ha ocurrido un error al intentar eliminar el circuito.");
            session.setAttribute("tipoAlerta", "error");
        };

        String location = request.getContextPath() + "/circuitos";
        response.sendRedirect(location);
    }

    private void listarCircuitos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        List<Circuito> circuitos = circuitoDAO.obtenerTodos();
        request.setAttribute("circuitos", circuitos);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/VistaCircuito.jsp");
        rd.forward(request, response);
    }

    private void mostrarFormularioCreacion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/VistaCircuitoForm.jsp");
        request.setAttribute("action", "insertar");
        dispatcher.forward(request, response);
    }

    private void mostrarFormularioEdicion(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        String paramValue = request.getParameter("id");
        Integer id = Integer.valueOf(paramValue);
        Circuito circuito = circuitoDAO.obtener(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/VistaCircuitoForm.jsp");
        request.setAttribute("circuito", circuito);
        request.setAttribute("action", "actualizar");
        dispatcher.forward(request, response);
    }
}
