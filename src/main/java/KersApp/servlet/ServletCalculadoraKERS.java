package KersApp.servlet;

import KersApp.modelo.Circuito;
import KersApp.modelo.Coche;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import KersApp.modelo.dao.CircuitoDAO;
import KersApp.modelo.dao.CocheDAO;
import java.util.List;

/**
 *
 * @author Javier González de Lope
 */
public class ServletCalculadoraKERS extends HttpServlet {

    private CircuitoDAO circuitoDAO;
    private CocheDAO cocheDAO;

    @Override
    public void init() {
        String urlDB = getServletContext().getInitParameter("urlDB");
        String usuarioDB = getServletContext().getInitParameter("usuarioDB");
        String claveDB = getServletContext().getInitParameter("claveDB");

        circuitoDAO = new CircuitoDAO(urlDB, usuarioDB, claveDB);
        cocheDAO = new CocheDAO(urlDB, usuarioDB, claveDB);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Circuito> circuitos;
        List<Coche> coches;
        try {
            circuitos = circuitoDAO.obtenerTodos();
            coches = cocheDAO.obtenerTodos();
            request.setAttribute("circuitos", circuitos);
            request.setAttribute("coches", coches);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/VistaCalculadoraKERS.jsp");
            rd.forward(request, response);
        } catch (SQLException ex) {
            System.out.println("Error en Servlet Ganancia:" + ex);
        }

    }
}
