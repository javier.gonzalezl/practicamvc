package KersApp.modelo;

/**
 *
 * @author javie
 */
public class Coche {
    private int id;
    private String nombre;
    private int gananciaPorCurva;

    public Coche() {
    }

    public Coche(int id) {
        this.id = id;
    }
    
    public Coche(int id, String nombre, int gananciaPorCurva) {
        this(nombre, gananciaPorCurva);
        this.id = id;
    }

    public Coche(String nombre, int gananciaPorCurva) {
        this.nombre = nombre;
        this.gananciaPorCurva = gananciaPorCurva;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getGananciaPorCurva() {
        return gananciaPorCurva;
    }

    public void setGananciaPorCurva(int gananciaPorCurva) {
        this.gananciaPorCurva = gananciaPorCurva;
    }
}
