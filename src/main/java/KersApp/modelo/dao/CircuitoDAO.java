package KersApp.modelo.dao;

import java.util.*;
import java.sql.*;
import KersApp.modelo.Circuito;

public class CircuitoDAO implements DAO<Circuito, Integer> {

    // parámetros de conexión a la BBDD
    private final String urlDB;
    private final String usuarioDB;
    private final String claveDB;
    private Connection conexion;

    // método constructor
    public CircuitoDAO(String urlDB, String usuarioDB, String claveDB) {
        this.urlDB = urlDB;
        this.usuarioDB = usuarioDB;
        this.claveDB = claveDB;
    }

    private void conectar() throws SQLException {
        if (conexion == null || conexion.isClosed()) {
            conexion = DriverManager.getConnection(urlDB, usuarioDB, claveDB);
        }
    }

    private void desconectar() throws SQLException {
        if (conexion != null && !conexion.isClosed()) {
            conexion.close();
        }
    }

    @Override
    public boolean insertar(Circuito circuito) throws SQLException {
        String sql = "INSERT INTO circuitos(nombre, ciudad, pais, "
                + "numVueltas, longVuelta, curvasVuelta) VALUES (?,?,?,?,?,?)";
        // Iniciar conexión a la BBDD
        conectar();
        boolean filaInsertada = false; 
        try (PreparedStatement stat = conexion.prepareStatement(sql)) {
            stat.setString(1, circuito.getNombre());
            stat.setString(2, circuito.getCiudad());
            stat.setString(3, circuito.getPais());
            stat.setInt(4, circuito.getNumVueltas());
            stat.setInt(5, circuito.getLongVuelta());
            stat.setInt(6, circuito.getCurvasVuelta());
            filaInsertada = stat.executeUpdate() > 0;
        } catch (SQLException ex) {

        } finally {
            // Cerrar conexión a la BBDD
            desconectar();
            return filaInsertada;
        }
    }

    @Override
    public boolean modificar(Circuito circuito) throws SQLException {
        String sql = "UPDATE circuitos SET nombre = ?, ciudad = ?, pais = ?, "
                + "numvueltas = ?, longVuelta = ?, curvasVuelta = ? WHERE id = ?";

        // Iniciar conexión a la BBDD
        conectar();

        boolean filaActualizada = false;
        try (PreparedStatement stat = conexion.prepareStatement(sql)) {
            stat.setString(1, circuito.getNombre());
            stat.setString(2, circuito.getCiudad());
            stat.setString(3, circuito.getPais());
            stat.setInt(4, circuito.getNumVueltas());
            stat.setInt(5, circuito.getLongVuelta());
            stat.setInt(6, circuito.getCurvasVuelta());
            stat.setInt(7, circuito.getId());
            filaActualizada = stat.executeUpdate() > 0;
        } catch (SQLException ex) {

        } finally {
            // Cerrar conexión a la BBDD
            desconectar();
            return filaActualizada;
        }
    }

    @Override
    public boolean eliminar(Circuito circuito) throws SQLException {
        String sql = "DELETE FROM circuitos WHERE id = ?";

        // Iniciar conexión a la BBDD
        conectar();

        boolean filaEliminada = false;
        try (PreparedStatement stat = conexion.prepareStatement(sql)) {
            stat.setInt(1, circuito.getId());
            filaEliminada = stat.executeUpdate() > 0;
        } catch (SQLException ex) {

        } finally {
            // Cerrar conexión a la BBDD
            desconectar();
            return filaEliminada;
        }
    }

    private Circuito convertir(ResultSet rs) throws SQLException {
        // Conversión de columnas
        int id = rs.getInt("id");
        String nombre = rs.getString("nombre");
        String ciudad = rs.getString("ciudad");
        String pais = rs.getString("pais");
        int numVueltas = rs.getInt("numVueltas");
        int longVuelta = rs.getInt("longVuelta");
        int curvasVuelta = rs.getInt("curvasVuelta");

        // Creación del objeto
        Circuito circuito = new Circuito(id, nombre, ciudad, pais, numVueltas,
                longVuelta, curvasVuelta);

        return circuito;
    }

    @Override
    public List<Circuito> obtenerTodos() throws SQLException {
        List<Circuito> listaCircuitos = new ArrayList<>();
        String sql = "SELECT * FROM circuitos";

        // Iniciar conexión a la BBDD
        conectar();

        try (Statement stat = conexion.createStatement();
                ResultSet rs = stat.executeQuery(sql)) {
            while (rs.next()) {
                listaCircuitos.add(convertir(rs));
            }
        }

        // Cerrar conexión a la BBDD
        desconectar();

        return listaCircuitos;
    }

    @Override
    public Circuito obtener(Integer id) throws SQLException {
        Circuito circuito = null;
        String sql = "SELECT * FROM circuitos WHERE id = ?";

        // Iniciar conexión a la BBDD
        conectar();

        try (PreparedStatement stat = conexion.prepareStatement(sql);) {
            stat.setInt(1, id);
            ResultSet rs = stat.executeQuery();
            if (rs.next()) {
                circuito = convertir(rs);
            }
        }

        // Cerrar conexión a la BBDD
        desconectar();

        return circuito;
    }
}
