package KersApp.modelo.dao;

import java.util.*;
import java.sql.*;
import KersApp.modelo.Coche;

public class CocheDAO implements DAO<Coche, Integer> {

    // parámetros de conexión a la BBDD
    private final String urlDB;
    private final String usuarioDB;
    private final String claveDB;
    private Connection conexion;

    // método constructor
    public CocheDAO(String urlDB, String usuarioDB, String claveDB) {
        this.urlDB = urlDB;
        this.usuarioDB = usuarioDB;
        this.claveDB = claveDB;
    }

    private void conectar() throws SQLException {
        if (conexion == null || conexion.isClosed()) {
            conexion = DriverManager.getConnection(urlDB, usuarioDB, claveDB);
        }
    }

    private void desconectar() throws SQLException {
        if (conexion != null && !conexion.isClosed()) {
            conexion.close();
        }
    }

    @Override
    public boolean insertar(Coche coche) throws SQLException {
        String sql = "INSERT INTO coches(nombre, ganancia) VALUES (?,?)";

        // Iniciar conexión a la BBDD
        conectar();
        boolean filaInsertada = false;
        try (PreparedStatement stat = conexion.prepareStatement(sql)) {
            stat.setString(1, coche.getNombre());
            stat.setInt(2, coche.getGananciaPorCurva());
            filaInsertada = stat.executeUpdate() > 0;
        } catch (SQLException ex) {

        } finally {
            // Cerrar conexión a la BBDD
            desconectar();
            return filaInsertada;
        }
    }

    @Override
    public boolean modificar(Coche coche) throws SQLException {
        String sql = "UPDATE coches SET nombre = ?, ganancia = ? WHERE id = ?";
        // Iniciar conexión a la BBDD
        conectar();

        boolean filaActualizada = false;
        try (PreparedStatement stat = conexion.prepareStatement(sql)) {
            stat.setString(1, coche.getNombre());
            stat.setInt(2, coche.getGananciaPorCurva());
            stat.setInt(3, coche.getId());
            filaActualizada = stat.executeUpdate() > 0;
        } catch (SQLException ex) {

        } finally {
            // Cerrar conexión a la BBDD
            desconectar();
            return filaActualizada;
        }
    }

    @Override
    public boolean eliminar(Coche coche) throws SQLException {
        String sql = "DELETE FROM coches WHERE id = ?";

        // Iniciar conexión a la BBDD
        conectar();

        boolean filaEliminada = false;
        try (PreparedStatement stat = conexion.prepareStatement(sql)) {
            stat.setInt(1, coche.getId());
            filaEliminada = stat.executeUpdate() > 0;
        } catch (SQLException ex) {

        } finally {
            // Cerrar conexión a la BBDD
            desconectar();
            return filaEliminada;
        }
    }

    private Coche convertir(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String nombre = rs.getString("nombre");
        int ganancia = rs.getInt("ganancia");
        Coche coche = new Coche(id, nombre, ganancia);
        return coche;
    }

    @Override
    public List<Coche> obtenerTodos() throws SQLException {
        List<Coche> listaCoches = new ArrayList<>();
        String sql = "SELECT * FROM coches";

        // Iniciar conexión a la BBDD
        conectar();

        try (Statement stat = conexion.createStatement();
                ResultSet rs = stat.executeQuery(sql)) {
            while (rs.next()) {
                listaCoches.add(convertir(rs));
            }
        }

        // Cerrar conexión a la BBDD
        desconectar();

        return listaCoches;
    }

    @Override
    public Coche obtener(Integer id) throws SQLException {
        Coche coche = null;
        String sql = "SELECT * FROM coches WHERE id = ?";

        // Iniciar conexión a la BBDD
        conectar();

        try (PreparedStatement stat = conexion.prepareStatement(sql);) {
            stat.setInt(1, id);
            ResultSet rs = stat.executeQuery();
            if (rs.next()) {
                coche = convertir(rs);
            }
        }

        // Cerrar conexión a la BBDD
        desconectar();

        return coche;
    }

}
