package KersApp.modelo.dao;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Javier González de Lope
 * @param <T> Clase que implementa la interfaz
 * @param <K> tipo de la clave primaria que identifica de forma unívoca los 
 * objetos de una clase.
 */
public interface DAO<T,K> {
    boolean insertar(T a) throws SQLException;
    
    boolean modificar(T a) throws SQLException;
    
    boolean eliminar (T a) throws SQLException;
    
    List<T> obtenerTodos() throws SQLException;
    
    T obtener(K id) throws SQLException;
}
