package KersApp.modelo;

/**
 *
 * @author javie
 */
public class Circuito {
    private int id;
    private String nombre;
    private String ciudad;
    private String pais;
    private int numVueltas;
    private int longVuelta;
    private int curvasVuelta;

    public Circuito() {
    }

    public Circuito(int id) {
        this.id = id;
    }

    public Circuito(int id, String nombre, String ciudad, String pais, 
            int numVueltas, int longVuelta, int curvasVuelta){
        this(nombre, ciudad, pais, numVueltas, longVuelta, curvasVuelta);
        this.id = id;
    }
    
    public Circuito(String nombre, String ciudad, String pais, int numVueltas, int longVuelta, int curvasVuelta) {
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.pais = pais;
        this.numVueltas = numVueltas;
        this.longVuelta = longVuelta;
        this.curvasVuelta = curvasVuelta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getNumVueltas() {
        return numVueltas;
    }

    public void setNumVueltas(int numVueltas) {
        this.numVueltas = numVueltas;
    }

    public int getLongVuelta() {
        return longVuelta;
    }

    public void setLongVuelta(int longVuelta) {
        this.longVuelta = longVuelta;
    }

    public int getCurvasVuelta() {
        return curvasVuelta;
    }

    public void setCurvasVuelta(int curvasPorVuelta) {
        this.curvasVuelta = curvasPorVuelta;
    }

    @Override
    public String toString() {
        return "Circuito{" + "id=" + id + ", nombre=" + nombre + ", ciudad=" + ciudad + ", pais=" + pais + ", numVueltas=" + numVueltas + ", longVuelta=" + longVuelta + ", curvasVuelta=" + curvasVuelta + '}';
    }
    
    
}
