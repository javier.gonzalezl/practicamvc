package KersApp.modelo;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

/**
 *
 * @author Javier González de Lope
 */
public class ConexionDB {

    //Parámetros de conexión
    public static final String USERNAME = "nbuser";
    public static final String PASSWORD = "nbuser";
    public static final String HOST = "localhost";
    public static final String PORT = "1527";
    public static final String DATABASE = "kers";
    // public static final String CLASSNAME = "org.apache.derby.jdbc.ClientDriver";
    public static final String URL = "jdbc:derby://" + HOST + ":" + PORT + "/"
            + DATABASE;

    private final Connection conexion;

    public ConexionDB() throws SQLException {
        //Class.forName(CLASSNAME); // A partir de jdbc 4 no es necesario
        conexion = DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }
    
    public Connection getConexion(){
        return conexion;
    }

    public void cerrarConexion() throws SQLException {
        conexion.close();
    }
}
