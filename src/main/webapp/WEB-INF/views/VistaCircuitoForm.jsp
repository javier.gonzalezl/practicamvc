<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Crear Circuito</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <!-- Bootstrap core CSS -->
        <link href="<c:out value='${pageContext.request.contextPath}'/>/css/bootstrap.min.css" rel="stylesheet" />
        <!-- Custom styles for this template -->

        <link href="<c:out value='${pageContext.request.contextPath}'/>/css/style.css" rel="stylesheet" />
        <link href="<c:out value='${pageContext.request.contextPath}'/>/css/all.min.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    </head>

    <body>
        <%@ include file="includes/navbar.jsp"%>

        <main role="main">
            <div class="container">
                <div class="row justify-content-center py-5 mx-auto">

                    <div class="col-md-8">
                        <h4 class="mb-3">
                            <c:if test="${circuito != null}">Editar circuito</c:if>
                            <c:if test="${circuito == null}">Crear circuito</c:if>
                            </h4>

                            <form action="<c:out value='${action}'/>" method="post" class="needs-validation" novalidate>
                            <c:if test="${circuito != null}">
                                <input type="hidden" name="id" value="<c:out value='${circuito.id}' />" />
                            </c:if>    
                            <div class="mb-3">
                                <label for="nombre">Nombre</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del circuito" value="<c:out value='${circuito.nombre}' />" required>
                                    <div class="invalid-feedback" style="width: 100%;">
                                        El nombre del circuito es obligatorio.
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="ciudad">Ciudad</label>
                                    <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value="<c:out value='${circuito.ciudad}' />" required>
                                    <div class="invalid-feedback">
                                        La ciudad del circuito es obligatoria.
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="pais">País</label>
                                    <input type="text" class="form-control" id="pais" name="pais" placeholder="País" value="<c:out value='${circuito.pais}' />" required>
                                    <div class="invalid-feedback">
                                        El país del circuito es obligatorio.
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <label for="numVueltas">Número de Vueltas</label>
                                    <input type="number" class="form-control" name="numVueltas" id="numVueltas" min="40" max="80" value="<c:out value='${circuito.numVueltas}' />" placeholder="40-80">
                                    <div class="invalid-feedback">
                                        Por favor, introduzca un número de vueltas entre 40 y 80.
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="longVuelta">Longitud de la vuelta</label>
                                    <input type="number" class="form-control" name="longVuelta" id="longVuelta" min="3000" max="9000" value="<c:out value='${circuito.longVuelta}' />" placeholder="3000-9000"
                                           required>
                                    <div class="invalid-feedback">
                                        Por favor, introduzca una longitud de vuelta entre 3000 y 9000.
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="curvasVuelta">Curvas por vuelta</label>
                                    <input type="number" class="form-control" name="curvasVuelta" id="curvasVuelta" min="6" max="20" value="<c:out value='${circuito.curvasVuelta}' />" placeholder="6-20"
                                           required>
                                    <div class="invalid-feedback">
                                        Por favor, introduzca un cantidad de curvas por vuelta entre 6 y 20.
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4 offset-md-10">
                                    <a href="<c:out value='${pageContext.request.contextPath}'/>/circuitos" class="btn btn-outline-primary">Atrás</a>
                                    <button class="btn btn-primary" type="submit">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main> 

        <%@ include file="includes/footer.jsp" %>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
    </body>
</html>
