<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html lang="es">
    <head>
        <title>Circuitos</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/all.min.css" rel="stylesheet" />
    </head>

    <body>
        <%@ include file="includes/navbar.jsp"%>
        <%
            String alerta = "", colorAlerta = "";
            if (session.getAttribute("alerta") != null) {
                alerta = session.getAttribute("alerta").toString();
                colorAlerta = session.getAttribute("tipoAlerta").toString() == "success" ? "success" : "danger";
                out.print("<div class=\"alert alert-" + colorAlerta + "\" role=\"alert\">");
                out.print(alerta);
                out.print("</div>");
            }

            session.removeAttribute("alerta");
            session.removeAttribute("tipoAlerta");
        %>
        <main role="main">

            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Gestión de Circuitos</h1>
                    <p class="lead text-muted">Desde aquí se puede visualizar la información de todos los circuitos
                        registrados en la aplicación. Además de esto, también se puede añadir un nuevo circuito y modificar
                        o eliminar uno ya existente</p>
                </div>
            </section>
            <div class="container mb-5">

                <div class="row ">
                    <div class="col-12 justify-content-end d-flex">
                        <a href="<c:out value='${pageContext.request.contextPath}'/>/circuitos/nuevo" class="btn btn-success"><i class="fas fa-plus"></i>   Añadir</a>
                    </div>
                </div>

                <div class="row mt-3">
                    <table class="table table-hover table-bordered text-center mx-auto">
                        <thead class="table-dark">
                            <tr>
                                <th>Nombre</th>
                                <th>Ciudad</th>
                                <th>País</th>
                                <th>NºVueltas</th>
                                <th>Longitud</th>
                                <th>Curvas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="circuito" items="${circuitos}">
                                <tr>
                                    <td><c:out value="${circuito.nombre}" /></td>
                                    <td><c:out value="${circuito.ciudad}" /></td>
                                    <td><c:out value="${circuito.pais}" /></td>
                                    <td><c:out value="${circuito.numVueltas}" /></td>
                                    <td><c:out value="${circuito.longVuelta}" /></td>
                                    <td><c:out value="${circuito.curvasVuelta}" /></td>
                                    <td>
                                        <ul class="list-inline m-0">
                                            <li class="list-inline-item">
                                                <a href="<c:out value='${pageContext.request.contextPath}'/>/circuitos/editar?id=<c:out value='${circuito.id}' />"><i class="fa fa-edit"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="<c:out value='${pageContext.request.contextPath}'/>/circuitos/eliminar?id=<c:out value='${circuito.id}' />"><i class="fa fa-trash"></i></a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </main>

        <%@ include file="includes/footer.jsp" %>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </body>

</html>
