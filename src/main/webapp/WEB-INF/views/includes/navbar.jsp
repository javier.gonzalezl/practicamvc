<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header>
    <nav class="navbar navbar-dark navbar-expand bg-dark">
        <div class="container">
            <a href="<c:out value='${pageContext.request.contextPath}'/>/index" class="navbar-brand">
                <span>KersApp</span>
            </a>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:out value='${pageContext.request.contextPath}'/>/index">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:out value='${pageContext.request.contextPath}'/>/circuitos">Circuitos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:out value='${pageContext.request.contextPath}'/>/coches">Coches</a>
                    </li>
                    <li class="nav-item">
                       <a class="nav-link" href="<c:out value='${pageContext.request.contextPath}'/>/calculadoraKERS">Calculadora KERS</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>