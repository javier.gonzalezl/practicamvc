<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Crear Coche</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <!-- Bootstrap core CSS -->
        <link href="<c:out value='${pageContext.request.contextPath}'/>/css/bootstrap.min.css" rel="stylesheet" />
        <!-- Custom styles for this template -->

        <link href="<c:out value='${pageContext.request.contextPath}'/>/css/style.css" rel="stylesheet" />
        <link href="<c:out value='${pageContext.request.contextPath}'/>/css/all.min.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    </head>

    <body>
        <%@ include file="includes/navbar.jsp"%>
        <main role="main">
            <div class="container">
                <div class="row justify-content-center py-5 mx-auto">
                    <div class="col-md-8">
                        <h4 class="mb-3">
                            <c:if test="${coche != null}">Editar coche</c:if>
                            <c:if test="${coche == null}">Crear coche</c:if>
                            </h4>
                            <form class="needs-validation" novalidate action="<c:out value='${action}'/>" method="post">
                            <c:if test="${coche != null}">
                                <input type="hidden" name="id" value="<c:out value='${coche.id}' />" />
                            </c:if>   
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="ciudad">Nombre</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<c:out value='${coche.nombre}' />" required>
                                    <div class="invalid-feedback">
                                        El nombre del coche es obligatorio.
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gananciaPorCurva">Ganancia por curva</label>
                                    <input type="number" class="form-control" id="gananciaPorCurva" name="gananciaPorCurva" min="4" max="10" placeholder="4 - 10" value="<c:out value='${coche.gananciaPorCurva}' />" required>
                                    <div class="invalid-feedback">
                                        Por favor, introduzca un cantidad de ganancia por curva comprendida entre 4 y 10.
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4 offset-md-10">
                                    <a href="<c:out value='${pageContext.request.contextPath}'/>/coches" class="btn btn-outline-primary">Atrás</a>
                                    <button class="btn btn-primary" type="submit">Guardar</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>


        <%@ include file="includes/footer.jsp" %>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
    </body>
</html>
