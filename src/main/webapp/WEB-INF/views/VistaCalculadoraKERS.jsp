<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML>
<html lang="es">
    <head>
        <title>Calculadora KERS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/all.min.css" rel="stylesheet" />
        <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    </head>

    <body>
        <%@ include file="includes/navbar.jsp"%>

        <main role="main">
            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Calculadora KERS</h1>
                    <p class="lead text-muted">Desde aquí se puede calcular la ganancia de potencia que tendrá cada vehículo 
                        circulando por un circuito concreto, gracias al sistema KERS que lleva instalado. Para ello, solamente
                    será necesario seleccionar un coche y un circuito y la aplicación calculará el resultado automáticamente.</p>
                </div>
            </section>
            <div class="container">
                <div class="row justify-content-center pt-5 mb-5">
                    <div class="col-md-6">
                        <label for="circuitos">Seleccione un circuito: </label>
                        <select class="form-control" id="circuitos" size="7">
                            <c:forEach var="circuito" items="${circuitos}">
                                <option 
                                    data-numVueltas="<c:out value="${circuito.numVueltas}" />"
                                    data-longVuelta="<c:out value="${circuito.longVuelta}" />"
                                    data-curvasVuelta="<c:out value="${circuito.curvasVuelta}" />"
                                    value="<c:out value="${circuito.nombre}" />"><c:out value="${circuito.nombre}" />
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col-md-4 offset-md-2">
                        <p><strong>Circuito seleccionado:</strong></p>
                        <p>- Nombre: <span id="nombreCircuito"></span></p>
                        <p>- Nº Vueltas: <span id="vueltas"></span></p>
                        <p>- Longitud de vuelta: <span id="longVuelta"></span>km</p>
                        <p>- Curvas por vuelta: <span id="curvasVuelta"></span></p>
                    </div>
                </div>
                <div class="row justify-content-center mb-5">
                    <div class="col-md-6">
                        <label for="coches">Seleccione un coche:</label>
                        <select class="form-control" id="coches" size="7">
                            <c:forEach var="coche" items="${coches}">
                                <option
                                    data-ganancia="<c:out value="${coche.gananciaPorCurva}" />"
                                    value="<c:out value="${coche.nombre}" />"><c:out value="${coche.nombre}" />
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col-md-4 offset-md-2">
                        <p><strong>Coche seleccionado:</strong></p>
                        <p>- Nombre: <span id="nombreCoche"></span></p>
                        <p>- Ganancia por curva: <span id="gananciaCurva"></span>kW</p>
                    </div>
                </div>
                <hr class="my-3">
                <div class="row justify-content-center mx-auto">
                    <div class="mb-3">
                        <p><strong>Energía generada:</strong></p>
                        <p>- Por vuelta: <span id="gananciaCocheSeleccionado"></span> x <span id="curvasCircuitoSeleccionado"></span> ----> <strong id="energiaVuelta"></strong> kW</p>
                        <p>- Total: <span id="energiaVuelta2"></span> x <span id="vueltasCircuitoSeleccionado"></span> ----> <strong id="energiaTotal"></strong> kW</p>
                    </div>
                </div>
            </div>

        </main>

        <%@ include file="includes/footer.jsp" %>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script src="js/calculadoraKers.js"></script>
    </body>
</html>
