const $circuitosSelect = document.querySelector('#circuitos');
const $cochesSelect = document.querySelector('#coches');
const $nombreCocheSpan = document.querySelector('#nombreCoche');
const $gananciaCocheSpan = document.querySelector('#gananciaCurva');
const $nombreCircuitoSpan = document.querySelector('#nombreCircuito');
const $vueltasCircuitoSpan = document.querySelector('#vueltas');
const $longitudCircuitoSpan = document.querySelector('#longVuelta');
const $curvasCircuitoSpan = document.querySelector('#curvasVuelta');
const $energiaVueltaSpan = document.querySelector('#energiaVuelta');
const $energiaTotalSpan = document.querySelector('#energiaTotal');
const $gananciaCocheSeleccionadoSpan = document.querySelector('#gananciaCocheSeleccionado');
const $curvasCircuitoSeleccionadoSpan = document.querySelector('#curvasCircuitoSeleccionado');
const $vueltasCircuitoSeleccionadoSpan = document.querySelector('#vueltasCircuitoSeleccionado');
const $energiaVuelta2Span = document.querySelector('#energiaVuelta2');

let circuito = {};
let coche = {};


function init() {
    // Seleccionamos por defecto la primera opción de cada selector
    $circuitosSelect.options[0].selected = "selected";
    $cochesSelect.options[0].selected = "selected";

    circuito = getCircuitObject();
    updateCircuitInfo(circuito);

    coche = getCarObject();
    updateCarInfo(coche);

    updateEnergyInfo(circuito, coche);
}

function getCarObject() {
    let $selectedCar = $cochesSelect.options[$cochesSelect.selectedIndex];

    let nombre = $selectedCar.text;
    let ganancia = $selectedCar.dataset.ganancia;

    let car = {
        nombre,
        ganancia: parseInt(ganancia)
    };

    return car;
}

function getCircuitObject() {
    let $selectedCircuit = $circuitosSelect.options[$circuitosSelect.selectedIndex];

    let nombre = $selectedCircuit.text;
    let vueltas = $selectedCircuit.dataset.numvueltas;
    let longitud = $selectedCircuit.dataset.longvuelta;
    let curvas = $selectedCircuit.dataset.curvasvuelta;

    let circuit = {
        nombre,
        vueltas: parseInt(vueltas),
        longitud: parseInt(longitud),
        curvas: parseInt(curvas)
    };

    return circuit;
}

function updateCarInfo(car) {
    $nombreCocheSpan.innerHTML = car.nombre;
    $gananciaCocheSpan.innerHTML = car.ganancia;
}

function updateCircuitInfo(circuit) {
    $nombreCircuitoSpan.innerHTML = circuit.nombre;
    $vueltasCircuitoSpan.innerHTML = circuit.vueltas;
    $longitudCircuitoSpan.innerHTML = circuit.longitud;
    $curvasCircuitoSpan.innerHTML = circuit.curvas;
}

function updateEnergyInfo(circuit, car) {
    let energiaVuelta = car.ganancia * circuit.curvas;
    let energiaTotal = energiaVuelta * circuit.vueltas;

    $gananciaCocheSeleccionadoSpan.innerHTML = car.ganancia;
    $curvasCircuitoSeleccionadoSpan.innerHTML = circuit.curvas;
    $energiaVueltaSpan.innerHTML = energiaVuelta;
    
    $vueltasCircuitoSeleccionadoSpan.innerHTML = circuit.vueltas;
    $energiaVuelta2Span.innerHTML = energiaVuelta;
    $energiaTotalSpan.innerHTML = energiaTotal;
}

init();

$circuitosSelect.addEventListener("change", () => {
    circuito = getCircuitObject();
    updateCircuitInfo(circuito);
    updateEnergyInfo(circuito, coche);
});


$cochesSelect.addEventListener("change", () => {
    coche = getCarObject();
    updateCarInfo(coche);
    updateEnergyInfo(circuito, coche);
});

