<%-- 
    Document   : index
    Created on : 14-dic-2020, 0:49:22
    Author     : Javier González de Lope
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>F1 2020/2021</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet" />
    </head>
    <body>
        <%@ include file="WEB-INF/views/includes/navbar.jsp"%>

        <main role="main">

            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Calculadora KERS - F1 Temporada 2021</h1>
                    <p class="lead text-muted">¡Bienvenido a su rincón de Formula 1 para la temporada 2021! En nuestra web
                        podrá gestionar los vehículos participantes, los circuitos donde tendrán lugar los grandes premios y
                        la ganancia de energía de cada vehículo en cada circuito.</p>
                </div>
            </section>

            <div class="album py-5 bg-dark">
                <div class="container">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card mb-4">
                                <a class="card-block stretched-link text-decoration-none" href="<c:out value='${pageContext.request.contextPath}'/>/coches">
                                    <img class="card-img-top" src="images/car.jpg" alt="Coche">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">Gestión de coches</h5>
                                        <p class="card-text text-muted">Añade, modifica o elimina los coches de la competición.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4">
                                <a class="card-block stretched-link text-decoration-none" href="<c:out value='${pageContext.request.contextPath}'/>/circuitos">
                                    <img class="card-img-top" src="images/circuit.jpg" alt="Circuito">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">Gestión de circuitos</h5>
                                        <p class="card-text text-muted">Añade, modifica o elimina los circuitos de la competición.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4">
                                <a class="card-block stretched-link text-decoration-none" href="<c:out value='${pageContext.request.contextPath}'/>/calculadoraKERS">
                                    <img class="card-img-top" src="images/calculadora.jpg" alt="Calculadora">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">Calculadora KERS</h5>
                                        <p class="card-text text-muted">Calcula la ganancia de potencia de los vehículos en los circuitos.</p>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </main>

        <%@ include file="WEB-INF/views/includes/footer.jsp" %>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </body>
</html>
